interface Config {
    apiSocket: string
}

const config: Config = {
    apiSocket: process.env.REACT_APP_SOCKET_HOST || 'http://localhost:3001'
}

Object.freeze(config);

export default config;