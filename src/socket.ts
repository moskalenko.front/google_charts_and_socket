import io from 'socket.io-client';
import config from './config';

const ioClient: SocketIOClient.Socket = io(`${config.apiSocket}`);
const socketConnect = () => {console.log('socket connected')};
const socketErr = (error: any) => {console.error('socket.io error', error)};
const socketDisconnect = () => {console.log('socket disconnected')};
const socketConnectErr = (error: any) => {
    ioClient.disconnect();
    console.error('socket.io connect_error', error);
    alert("Socket connection fails. Please, check socket connection and refresh the page.");
};

ioClient.on('connect', socketConnect);
ioClient.on('connect_error', socketConnectErr);
ioClient.on('error', socketErr);
ioClient.on('disconnect', socketDisconnect);

export default ioClient;