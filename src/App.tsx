import React from 'react';
import ChartsContainer from './containers/ChartsContainer';

const App: React.FC = () => {
  return (
    <div className="App">
      <ChartsContainer />
    </div>
  );
}

export default App;
