import React from 'react';
import ioClient from '../socket';
import Chart from 'react-google-charts';
import numberFloor from '../utils/numberFloor';

interface SocketData {
    value: number,
    timestamp: number
};
interface State {
    dataLineChart: (string | number)[][],
    dataBarChart: (string | number)[][]
};

class ChartsContainer extends React.Component <{}, State> {
    state = {
        dataLineChart: [['time', 'value']],
        dataBarChart: [['category', 'amount']]
    }
    componentDidMount(){
        ioClient.on('data', this.saveData)
    }
    componentWillUnmount(){
        ioClient.removeListener('data', this.saveData)
    }
    saveData = (data: SocketData) => {
        const { dataLineChart } = this.state;
        const { createDataForBar } = this;
        const result = createDataForBar(data.value);

        this.setState({
            dataLineChart: [
                ...dataLineChart,
                [data.timestamp, data.value]
            ],
            dataBarChart: [
                ['category', 'amount'],
                ...result
            ]
        });
    }
    createDataForBar = (value: number) => {
        const { dataBarChart } = this.state;
        const valueFloor = numberFloor(value);
        const data: (string | number)[][] = dataBarChart.slice(1);
        const newLine = [`${valueFloor}:${valueFloor + 10}`, 1];

        if(data.length === 0) {
            data.push(newLine);

            return data;
        } else {
            for(let i = 0; i < data.length; i++){
                const line = data[i];
                const prevLine = data[i - 1];
                const nextLine = data[i + 1];
                const coast = String(line[0]).split(':');
                const min = +coast[0];
                const max = +coast[1];

                const coastNextLine = nextLine && String(nextLine[0]).split(':');
                const nextLineMin = coastNextLine && +coastNextLine[0];
            
                if(prevLine === undefined && value < min){
                    data.unshift(newLine);
                    break;
                }  else if(min <= value && value <= max){
                    line[1] = +line[1] + 1;
                    break;
                } else if(
                    nextLine !== undefined &&
                    value > max && 
                    value < nextLineMin
                ){
                    data.splice(i+1, 0, newLine);
                    break;
                } else if(nextLine === undefined && value > max){
                    data.push(newLine);
                    break;
                }
            }
        }

        return data;
    }
    render() {
        const { dataLineChart, dataBarChart } = this.state;

        return(
            <div style={{padding: '20px'}}>
                <div style={{width: '100%'}}>
                    {
                        dataLineChart.length > 1 ? (
                            <Chart
                                width={'100%'}
                                height={'400px'}
                                chartType="LineChart"
                                loader={<div>Loading Chart</div>}
                                data={dataLineChart}
                                rootProps={{ 'data-testid': '1' }}
                            />
                        ) : 'Loading...'
                    }
                </div>
                <div style={{width: '100%'}}>
                    {
                        dataBarChart.length > 1 ? (
                            <Chart
                                width={'100%'}
                                height={'400px'}
                                chartType="Bar"
                                loader={<div>Loading Chart</div>}
                                data={dataBarChart}
                            />
                        ) : 'Loading...'
                    }
                </div>
            </div>
        )
    }
}

export default ChartsContainer;